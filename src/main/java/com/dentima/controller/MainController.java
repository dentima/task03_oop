package com.dentima.controller;

import com.dentima.model.Plane;
import java.util.List;

public interface MainController {
  List<Plane> getAllPlanes();
  int getSumCapacity();
  int getSumLift();
  List<Plane> sortByRange();
  List<Plane> searchByFuel(int startPoint, int endPoint);
}
