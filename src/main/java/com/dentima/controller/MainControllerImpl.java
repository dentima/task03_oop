package com.dentima.controller;

import com.dentima.model.Airlane;
import com.dentima.model.Plane;
import java.util.List;

public class MainControllerImpl implements MainController {
  private Airlane airlane;

  public MainControllerImpl() {
    airlane = new Airlane();
  }

  @Override
  public List<Plane> getAllPlanes() {
    return airlane.getPlanes();
  }

  @Override
  public int getSumCapacity() {
    return airlane.getSumCapacity();
  }

  @Override
  public int getSumLift() {
    return airlane.getSumLift();
  }

  @Override
  public List<Plane> sortByRange() {
    return airlane.sortByRange();
  }

  @Override
  public List<Plane> searchByFuel(int startPoint, int endPoint) {
    return airlane.searchByFuel(startPoint, endPoint);
  }
}
