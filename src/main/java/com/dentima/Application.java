package com.dentima;

import com.dentima.view.View;

public class Application {
  public static void main(String[] args) {
    new View().show();
  }
}
