package com.dentima.model;

public class Plane {
  private String name;
  private PlaneType type;
  private Integer capacity;
  private Integer lift;
  private Integer fuel;
  private Integer range;

  public Plane() {
  }

  public Plane(String name, PlaneType type, Integer capacity, Integer lift, Integer fuel,
      Integer range) {
    this.name = name;
    this.type = type;
    this.capacity = capacity;
    this.lift = lift;
    this.fuel = fuel;
    this.range = range;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public PlaneType getType() {
    return type;
  }

  public void setType(PlaneType type) {
    this.type = type;
  }

  public Integer getCapacity() {
    return capacity;
  }

  public void setCapacity(Integer capacity) {
    this.capacity = capacity;
  }

  public Integer getLift() {
    return lift;
  }

  public void setLift(Integer lift) {
    this.lift = lift;
  }

  public Integer getFuel() {
    return fuel;
  }

  public void setFuel(Integer fuel) {
    this.fuel = fuel;
  }

  public Integer getRange() {
    return range;
  }

  public void setRange(Integer range) {
    this.range = range;
  }

  @Override
  public String toString() {
    return "Plane{" +
        "name = '" + name + '\'' +
        ", type = " + type +
        ", capacity = " + capacity +
        ", lift = " + lift +
        ", fuel = " + fuel +
        ", range = " + range +
        "}";
  }
}
