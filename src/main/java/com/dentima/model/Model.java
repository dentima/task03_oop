package com.dentima.model;

import java.util.List;

public interface Model {
  List<Plane> getPlanes();
  int getSumCapacity();
  int getSumLift();
  List<Plane> sortByRange();
  List<Plane> searchByFuel(int startPoint, int endPoint);
}
