package com.dentima.model;

import com.dentima.util.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


public class Airlane implements Model{
  private List<Plane> planes = new ArrayList<Plane>();

  public Airlane() {
    FileReader flowerIO= new FileReader("planes.json");
    try {
      planes=flowerIO.readFile();
    }catch (IOException e){
      System.err.print(e);
    }
  }

  public int getSumCapacity() {
    int sumCapacity = planes.stream().map(Plane::getCapacity).mapToInt(Integer::intValue).sum();
    return sumCapacity;
  }

  public int getSumLift() {
    int sumLift = planes.stream().map(Plane::getLift).mapToInt(Integer::intValue).sum();
    return sumLift;
  }

  @Override
  public List<Plane> sortByRange() {
    sort(planes);
    return planes;
  }

  @Override
  public List<Plane> searchByFuel(int startPoint, int endPoint) {
    List<Plane> sortedList = new ArrayList<>();
    for (Plane plane : planes){
      if (plane.getFuel() > startPoint && plane.getFuel() < endPoint){
        sortedList.add(plane);
      }
    }
    return sortedList;
  }

  public void sort(List<Plane> planes) {
    planes.sort(new Comparator<Plane>() {
      public int compare(Plane a, Plane b) {
        int compare = a.getRange().compareTo(b.getRange());
        if (compare == 0) {
          return 0;
        }
        if (compare > 0) {
          return 1;
        }
        return -1;
      }
    });
  }

  public List<Plane> getPlanes() {
    return planes;
  }

  public void setPlanes(List<Plane> planes) {
    this.planes = planes;
  }

  @Override
  public String toString() {
    return "Airlane{" +
        "planes=" + planes +
        '}';
  }
}
