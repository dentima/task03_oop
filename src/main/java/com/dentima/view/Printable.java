package com.dentima.view;

@FunctionalInterface
public interface Printable {
  void print();
}
