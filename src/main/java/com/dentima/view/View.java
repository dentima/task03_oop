package com.dentima.view;

import com.dentima.controller.MainController;
import com.dentima.controller.MainControllerImpl;
import com.dentima.model.Airlane;
import com.dentima.model.Plane;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
  private Airlane airlane = new Airlane();
  private MainController controller;
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Scanner input = new Scanner(System.in);

  public View() {
    controller = new MainControllerImpl();
    menu = new LinkedHashMap<>();
    menu.put("1", "  1 - Print all planes in the airline");
    menu.put("2", "  2 - Calculate total capacity and load capacity.");
    menu.put("3", "  3 - Sorting company planes by flight range.");
    menu.put("4", "  4 - Find a plane that fits a given range of fuel consumption options.");
    menu.put("Q", "  Q - exit");

    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::pressButton1);
    methodsMenu.put("2", this::pressButton2);
    methodsMenu.put("3", this::pressButton3);
    methodsMenu.put("4", this::pressButton4);
  }

  private void pressButton1() {
    for (Plane plane : controller.getAllPlanes()) {
      System.out.println(plane);
    }
  }

  private void pressButton2() {
    System.out.println("Total capacity - " + controller.getSumCapacity()
        + ", Load capacity - " + controller.getSumLift());
  }

  private void pressButton3() {
    for (Plane plane : controller.sortByRange()) {
      System.out.println(plane);
    }
  }

  private void pressButton4() {
    System.out.println("Please input start point of limit");
    int startPoint =input.nextInt();
    System.out.println("Please input end point of limit");
    int endPoint = input.nextInt();
    for (Plane plane : controller.searchByFuel(startPoint, endPoint)) {
      System.out.println(plane);
    }
  }

  private void outputMenu() {
    System.out.println("\nMENU:");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println("Please, select menu point.");
      keyMenu = input.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("Q"));
  }
}
