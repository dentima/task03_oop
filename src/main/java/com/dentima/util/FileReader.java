package com.dentima.util;

import com.dentima.model.Plane;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileReader {
  private static ObjectMapper objectMapper = new ObjectMapper();

  static {
    objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
  }

  private File file;

  public FileReader (String path) {
    this.file = new File(path);
  }

  public List<Plane> readFile() throws IOException {
    return objectMapper.readValue(file, objectMapper.getTypeFactory().constructCollectionType(
        ArrayList.class, Plane.class));
  }

  public void writeToFile(List<Plane> ammunitionList) throws IOException {
    objectMapper.writeValue(file, ammunitionList);
  }
}
